// TODO: pass array of regs to same function rather than one reg to each func
// TODO: will INS_OperandReg be any useful?
// TODO: QUEUE? some data structure in which bubble/reg to insert in every func call, automagically remove the last (stale) entry (keep only top 100)
// TODO: do some op on the arrays/counters so that they stay in cache
// FIXME: should inserts be in Instrumentation or Analysis
// TODO number of registers on system if no way to find, make it a macro

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <assert.h>
#include <string.h>
#include <set>
#include "pin.H"

#define NUM_REGS 27

ofstream OutFile;
FILE *pMyOutfile;

// The array storing the spacing frequency between two dependant instructions
UINT64 *dependancySpacing;

// arr[i] stores instruction num when reg i was written to
UINT64 *regLastWrite;

// Output file name
UINT32 maxSize;

// instruction number counter (in execution, not source code)
UINT64 insCounter;

// This knob sets the output file name
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool", "o", "result.csv", "specify the output file name");

// This knob will set the maximum spacing between two dependant instructions in the program
KNOB<string> KnobMaxSpacing(KNOB_MODE_WRITEONCE, "pintool", "s", "100", "specify the maximum spacing between two dependant instructions in the program");

set<REG> insReadRegs;
set<REG> insWriteRegs;

// This function is called before every instruction is executed. Have to change
// the code to send in the register names from the Instrumentation function
VOID updateSpacingInfo()
{
    fprintf(pMyOutfile, "ins #%lu\n", insCounter);
    fprintf(pMyOutfile, "read %lu regs, write %lu regs\n", insReadRegs.size(), insWriteRegs.size());

    set<REG>::iterator it;

    fprintf(pMyOutfile, "read: ");
    for (it=insReadRegs.begin(); it!=insReadRegs.end(); ++it) {
        fprintf(pMyOutfile, "%d ", *it);

        // check for dependancy in read regs
        if (insCounter - regLastWrite[*it] < maxSize)
        {
            dependancySpacing[ insCounter - regLastWrite[*it] ]++;
        }
    }
    fprintf(pMyOutfile, "\n");

    fprintf(pMyOutfile, "wrote: ");
    for (it=insWriteRegs.begin(); it!=insWriteRegs.end(); ++it) {
        fprintf(pMyOutfile, "%d ", *it);
        regLastWrite[*it] = insCounter;
    }
    fprintf(pMyOutfile, "\n");

    insCounter++;
}

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v)
{
    // update read regs
    insReadRegs.clear();
    for (UINT8 i = 0; i < INS_MaxNumRRegs(ins); ++i)
    {
        insReadRegs.insert( REG_FullRegName( INS_RegR(ins,i) ) );
    }

    // update write regs
    insWriteRegs.clear();
    for (UINT8 i = 0; i < INS_MaxNumWRegs(ins); ++i)
    {
        insWriteRegs.insert( REG_FullRegName( INS_RegW(ins,i) ) );
    }

    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)updateSpacingInfo, IARG_END);
}

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    // Write to a file since cout and cerr maybe closed by the application
    OutFile.open(KnobOutputFile.Value().c_str());
    OutFile.setf(ios::showbase);
	for(UINT32 i = 0; i < maxSize; i++)
		OutFile << dependancySpacing[i]<<",";
    OutFile.close();

    fclose(pMyOutfile);
}

// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    pMyOutfile = fopen("myoutput.txt", "w");

    // initialize variables
    // all regs were written to at time 0, therefore first instruction is at time 1
    insCounter = 1;
    regLastWrite = new UINT64[NUM_REGS];
    for (UINT8 i = 0; i < NUM_REGS; i++)
    {
        regLastWrite[i] = 0;
    }

    // Initialize pin
    PIN_Init(argc, argv);

    maxSize = atoi(KnobMaxSpacing.Value().c_str());

    // Initializing dependancy Spacing
    dependancySpacing = new UINT64[maxSize];

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
